#!/bin/bash -ex

shopt -s expand_aliases

component=$1
image_name="my-image"
container_name="my-container"

echo "running docker"

docker build --build-arg COMPONENT=$component -t $image_name -f $component/Dockerfile .

alias run_container="docker run --rm --name $container_name $image_name"

echo "start test"

run_container -m unittest discover -s $component/tests -p "*_test.py"

echo "end test"

echo "run"

# Here we can do ssh and cyber
run_container -m $component

echo "done run"

echo "Done building $component!"
