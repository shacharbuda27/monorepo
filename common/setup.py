from distutils.core import setup
import os

setup(name='common',
  version='1.0',
  description='Python Distribution Utilities',
  author='Greg Ward',
  author_email='gward@python.net',
  url='https://www.python.org/sigs/distutils-sig/',
  packages=['common'],
  install_requires=[
    'numpy',
  ],
)