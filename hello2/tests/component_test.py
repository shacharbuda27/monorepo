import numpy as np
from unittest import TestCase, main

class MyTestSuite(TestCase):
  def test_moti(self):
    self.assertEqual(1, 1)

  def test(self):
    self.assertTrue(True)

  def test_np(self):
    actual = np.array([1,2,3]).tolist()
    expected = [1, 2, 3]

    self.assertListEqual(actual, expected)

if __name__ == '__main__':
    main()